### WebParam
A simple static web page that connects to a device via WebSocket.

#### Usage
Load the webpage from [index.html](index.html) or at [misc.nnvtn.ca/webparam](misc.nnvtn.ca/webparam). Enter the IP address of the device running Param and click connect. The device should then push its parameters and the user interface will be populated according to the OSC paths of the parameters. If nothing happens try reloading the page and reconnecting.

You can then interact with your device. A line at the bottom shows you what messages are sent to the device. If the connection is lost, the inputs become grayed out. To reconnect reload the page and click connect.


#### JSON spec
To add parameters to the UI, the device sends a JSON message for each parameter.
```
{
    "add": {
        "addr": "/a/bang",
        "type": "bang"
    }
}
```
```
{
    "add": {
        "addr": "/a/int",
        "type": "slider",
        "min": 0,
        "max": 255,
        "def": 100,
        "v": 174
    }
}
```

#### JSON Control Message
When using the user interface WebParam sends json messages back to the device :
```
{
    "addr": "/a/int",
    "v": "90"
}
```

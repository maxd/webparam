#ifndef PARAM_WEBSOCKETS_H
#define PARAM_WEBSOCKETS_H

#include "ParamCollector.h"
#include "ParamJson.h"
#include "ArduinoWebsockets.h"

#ifndef PARAM_WS_PORT
 #define PARAM_WS_PORT 80
#endif

#ifndef PARAM_WS_MAX_CLIENT
 #define PARAM_WS_MAX_CLIENT 4
#endif

#ifndef WS_PUSH_PARAM_DELAY_MS 
#define WS_PUSH_PARAM_DELAY_MS 2
#endif

void updateParamWebsocket();
void paramWebsocketInit(ParamCollector* _collector);
void paramWebsocketPushParameter(Param * _p);
#endif

#include "ParamWebsockets.h"

using namespace websockets;

ParamCollector * paramColPtr;
WebsocketsClient socketClients[PARAM_WS_MAX_CLIENT];
WebsocketsServer socketServer;

// exposed
void paramWebsocketInit(ParamCollector* _collector){
    paramColPtr = _collector;
    socketServer.listen(PARAM_WS_PORT);
    if (!socketServer.available()) {
      Serial.println("[param] Websockets Server not available!");
    }
}

int8_t getFreeSocketClientIndex() {
    for (byte i = 0; i < PARAM_WS_MAX_CLIENT; i++) {
        if (!socketClients[i].available()) {
            return i;
        }
    }
    return -1;
}

void handleMessage(WebsocketsClient &client, WebsocketsMessage message) {
    auto data = message.data();
    if(data.length() > 0) {
        // deserialize message
        DeserializationError err = deserializeJson(paramJsonDoc, data);
        if(err){
            Serial.printf("[param] json fail :%s\n", err.c_str());
        }
        else {
            // receive the message
            JsonObject _obj = paramJsonDoc.as<JsonObject>();
            receiveJson(_obj, paramColPtr);
        }
    }
    // Log message
    // Serial.print("Got Message: ");
    // Serial.println(data);
}

void handleEvent(WebsocketsClient &client, WebsocketsEvent event, String data) {
    // Serial.printf("got : %s\n", data);
    switch(event){
        case WebsocketsEvent::ConnectionClosed:
            Serial.println("[param] ws connection closed");
            client.close();
            break;
        case WebsocketsEvent::ConnectionOpened:
            Serial.println("[param] ws connection opened");
            break;
        case WebsocketsEvent::GotPing:
            Serial.println("[param] ws got ping");
            break;
        case WebsocketsEvent::GotPong:
            Serial.println("[param] ws got pong");
            break;
    }
}


void pushParameter(Param * _p, int _clientIdx, bool _all){
    // make blank param object with the add add key
    paramJsonDoc.clear();
    JsonObject jsonParam = paramJsonDoc.createNestedObject("add");
    // compose a JsonObject with a parameter
    jsonifyParam(jsonParam, _p, true);
    // send the json over websocket
    size_t len = measureJson(paramJsonDoc)+1;
    char tmp[len];
    serializeJson(paramJsonDoc, tmp, len);
    if(_all){
        for(int i = 0; i < PARAM_WS_MAX_CLIENT; i++){
            if(socketClients[i].available()){
                socketClients[i].send(tmp);
            }
        }
    }
    else if(socketClients[_clientIdx].available()){
        socketClients[_clientIdx].send(tmp);
    }

}

void paramWebsocketPushParameter(Param * _p){
    // puh to all
    pushParameter(_p,0,true);
}



void listenForSocketClients() {
    if (socketServer.poll()) {
        Serial.println("[param] ws connection attempt");
        int8_t freeIndex = getFreeSocketClientIndex();
        if (freeIndex >= 0) {
            socketClients[freeIndex] = {};
            socketClients[freeIndex] = socketServer.accept();
            socketClients[freeIndex].onMessage(handleMessage);
            socketClients[freeIndex].onEvent(handleEvent);
            // when we get a new client we can push all parameters
            // Serial.printf("Accepted new websockets client at index %d\n", freeIndex);
            for(int i = 0; i < paramColPtr->index; i++) {
                pushParameter(paramColPtr->pointers[i], freeIndex, false);
                delay(WS_PUSH_PARAM_DELAY_MS);
            }
        }
    }
}


void pollSocketClients() {
    for (byte i = 0; i < PARAM_WS_MAX_CLIENT; i++) {
        socketClients[i].poll();
    }
}

// exposed
void updateParamWebsocket() {
    listenForSocketClients();
    pollSocketClients();
}

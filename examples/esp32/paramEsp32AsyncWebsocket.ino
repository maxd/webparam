/*
 * A websocket example for a ESP32
 * Serves as testing code
 */
#include <WiFi.h>
#include "Param.h"
#include "ParamJson.h"
#include "ParamOsc.h"

#include <ESPAsyncWebServer.h>
#define PARAM_WS_PORT 80
AsyncWebServer server(PARAM_WS_PORT);
AsyncWebSocket ws("/webparam");

// fill in your wifi credentials
#define WIFI_SSID "aziz-net"
#define WIFI_PSWD "killdash9"

#define LED_PIN BUILTIN_LED

// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;
// we include params to control the device
BangParam rebootButtonParam;

// EnumParam is ment to work with an enum
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};

// an array of the values
const int enumValues[] = {FIRST, SECOND, THIRD};
// and the human readable strings
const char* enumStrings[] = {"first", "second", "third"};

// a param collector to collect all parameters
ParamCollector paramCollector;

#define PARAM_WEBSOCKET_BUFFER_SIZE 256
char websocketMessageBuffer[PARAM_WEBSOCKET_BUFFER_SIZE];
void websocketEventHandler(
        AsyncWebSocket * server, 
        AsyncWebSocketClient * client, 
        AwsEventType type, 
        void * arg, 
        uint8_t *data, 
        size_t len
    ){
    switch(type){
        case WS_EVT_CONNECT:
            Serial.println("[ws] connected");
            for(int i = 0; i < paramCollector.index; i++) {
                paramJsonDoc.clear();
                JsonObject jsonParam = paramJsonDoc.createNestedObject("add");
                // compose a JsonObject with a parameter
                jsonifyParam(jsonParam, paramCollector.pointers[i], true);
                // send the json over websocket
                size_t len = measureJson(paramJsonDoc)+1;
                if(len < PARAM_WEBSOCKET_BUFFER_SIZE) {
                    serializeJson(paramJsonDoc, websocketMessageBuffer, len);
                    client->text(websocketMessageBuffer);    
                }
            }
            break;
        case WS_EVT_DATA:
            {
                DeserializationError err = deserializeJson(paramJsonDoc, data);
                if(err){
                    Serial.printf("[ws] json fail :%s\n", err.c_str());
                }
                else {
                    // receive the message
                    JsonObject _obj = paramJsonDoc.as<JsonObject>();
                    receiveJson(_obj, &paramCollector);
                }
            }
            break;
        case WS_EVT_DISCONNECT:
            Serial.println("[ws] disconnected");
            break;
    }
}

void setupParameters(){
    aFloatParam.set("/in/numbers/float", 0, 1, 0.5);
    aFloatParam.setCallback(floatCallback);
    aFloatParam.saveType = SAVE_ON_REQUEST;

    aIntParam.set("/in/numbers/int", 0, 255, 100);
    aIntParam.setCallback(intCallback);
    aIntParam.saveType = SAVE_ON_REQUEST;

    aColorParam.set("/in/color", 0);
    aColorParam.setCallback(colorCallback);
    aColorParam.saveType = SAVE_ON_REQUEST;

    aBangParam.set("/in/buttons/bang");
    aBangParam.setCallback(bangFunction);
    aBangParam.saveType = SAVE_ON_REQUEST;

    aBoolParam.set("/in/buttons/bool", 0);
    aBoolParam.setCallback(ledCallback);
    aBoolParam.saveType = SAVE_ON_REQUEST;

    aTextParam.set("/in/text", "foo");
    aTextParam.setCallback(textCallback);
    aTextParam.saveType = SAVE_ON_REQUEST;

    aEnumParam.set("/in/enum", enumStrings, enumValues, 3, SECOND);
    aEnumParam.setCallback(enumCallback);
    aEnumParam.saveType = SAVE_ON_REQUEST;


    rebootButtonParam.set("/config/reboot");
    rebootButtonParam.setCallback(reboot);

    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);
    //
    paramCollector.add(&rebootButtonParam);
}

void setup(){
    pinMode(LED_PIN, OUTPUT);
    Serial.begin(9600);
    while(!Serial);
    Serial.println("[boot] connected via serial");
    pinMode(LED_PIN, OUTPUT);

    // set parameters
    setupParameters();
    

    // connect to wifi
    WiFi.begin(WIFI_SSID, WIFI_PSWD);
    Serial.print("[wifi] Connecting to ");
    Serial.println(" ...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print('.');
    }
	
    Serial.println('\n');
    Serial.println("[wifi] connection established!");
    Serial.print("[wifi] IP address:\t");
    Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
    udpOscSocket.begin(udpOscPort);

    ws.onEvent(websocketEventHandler);
    server.addHandler(&ws);
    server.begin();
       // start the websocket handler
    Serial.println("[boot] done booting");
}



void loop(){
}

void weblog(const char * str){
    // if(logger.webLogAvailable() && ws.count() > 0){
    //     paramJsonDoc.clear();
    //     JsonObject jsonLog = paramJsonDoc.createNestedObject("log");
    //     jsonLog["text"] = webLog;
    //         // send the json over websocket
    //     size_t len = measureJson(paramJsonDoc)+1;
    //     if(len < PARAM_WEBSOCKET_BUFFER_SIZE) {
    //         if(ws.availableForWriteAll()){
    //             serializeJson(paramJsonDoc, websocketMessageBuffer, len);
    //             ws.textAll(websocketMessageBuffer);
    //             logger.clearWebLog();
    //         }
    //         else {
    //             logger.println("[ws] not avail");
    //         }
    //     }
    //     else {
    //         logger.println("[ws] message too big for buffer");
    //         logger.clearWebLog();
    //     }
    // }

}

// reboot callback
void reboot(){
    ESP.restart();
}


// These are all the callbacks for the parameters, they are optionnal
void bangFunction(){
    Serial.println("bang!");
}

void ledCallback(bool i){
    digitalWrite(LED_PIN, i);
    Serial.printf("bool : %i\n", i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", _i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(int _v) {
    Serial.printf("enum : %i %s\n", _v, enumStrings[_v]);
}

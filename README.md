WORK IN PROGRESS! 
### WebParam
With the advent of TCP/IP connected microcontrollers, parameters can be controlled via a web interface. WebParam is a static webpage to interact with the [Param](https://gitlab.com/maxd/param) system through a websocket server on the microcontroller. For ESP32, everything is already setup with [Esparam](https://gitlab.com/maxd/esparam)

Enter your device IP and click connect. A websocket connection will be opened and your device will list the parameters it has. WebParam adds the parameters as a cascading user interface based on the OSC address path of each parameter.

This is in use for quite a few project but moving forward we will implement this using OSCQuery, and lots of other cool new ways to play with your devices on your network!

<!-- Currently this is supported on the ESP8266, ESP32, and Teensy 4.1. Wiznet modules are also supported via a different library and some bulky code, see [wiznet example](examples/param_wiznet_websocket_example). -->